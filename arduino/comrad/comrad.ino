#include <SPI.h>
#include <stdarg.h>
#include <stdio.h>

#define CB_SPI_CLOCK 10000U

#define CB_SCK    13
#define CB_SO     12
#define CB_SI     11
#define CB_PERM   3
#define CB_MUTE   2


#define CB_CMD_GET_STEREO_STATUS 0xC2
#define CB_CMD_SEND_RADIO_STATUS 0x42

#define CB_STEREO_REQUEST_CHANNEL 0x0
#define CB_STEREO_REQUEST_VOLUME  0x1
#define CB_STEREO_REQUEST_SQUELCH 0x2

#define CB_STEREO_REQUEST_PARAM_LSB (1 << 7U)
#define CB_STEREO_REQUEST_PARAM_MASK 0x1F

#define CB_RADIO_STATUS_CHANNEL    0x1
#define CB_RADIO_STATUS_VOLUME     0x2
#define CB_RADIO_STATUS_SQUELCH    0x4

#define RADIO_STATUS_TX_BIT (1 << 5U)
#define RADIO_STATUS_RX_BIT (1 << 6U)

//#define LED       13

void debug_print(const char* format, ...)
{
    static char buffer[32U];
    va_list args;

    va_start (args, format);
    vsnprintf (buffer, sizeof(buffer), format, args);
    buffer[sizeof(buffer) - 1U] = '\x0';
    va_end (args);    

    Serial.write(buffer);
}

void cb_spi_init(void)
{
    pinMode(CB_SCK, OUTPUT);
    digitalWrite(CB_SCK, HIGH);

    pinMode(CB_SI, OUTPUT);
    digitalWrite(CB_SI, HIGH);
    
    pinMode(CB_MUTE, OUTPUT);
    digitalWrite(CB_MUTE, LOW);

    pinMode(CB_SO, INPUT);
}

uint8_t cb_spi_transfer(uint8_t tx_data)
{
    uint8_t rx_data = 0;

    uint8_t mask = 0x80U;

    while(mask)
    {
        digitalWrite(CB_SI, (tx_data & mask) ? HIGH : LOW);
        if(digitalRead(CB_SO) == HIGH)
        {
           rx_data |= mask;      
        }
        
        digitalWrite(CB_SCK, LOW);
        delayMicroseconds(1000000U / CB_SPI_CLOCK / 2U);

        digitalWrite(CB_SCK, HIGH);
        delayMicroseconds(1000000U / CB_SPI_CLOCK / 2U);

        mask >>= 1;
    }

    digitalWrite(CB_SI, LOW);

    return rx_data;
}

void setup() 
{
    Serial.begin(115200);            
    cb_spi_init();

    pinMode(CB_PERM, INPUT);

    //SPI.begin();
    //SPI.setClockDivider(SPI_CLOCK_DIV128);
    //SPI.beginTransaction(SPISettings(8000, MSBFIRST, SPI_MODE0)) ;

    Serial.write("===== Device Started =====\n"); 
}

void cb_write_radio_status(uint8_t status, uint8_t param, bool rx, bool tx)
{
    uint8_t byte1;
    uint8_t byte2;

    byte1 = (1 << 4) | status;
    if(tx)
    {
      byte1 |= RADIO_STATUS_TX_BIT;
    }

    if(rx)
    {
      byte1 |= RADIO_STATUS_RX_BIT;
    }
    


    byte2 = param;

    cb_spi_transfer(CB_CMD_SEND_RADIO_STATUS);
    cb_spi_transfer(byte1);
    cb_spi_transfer(byte2);
}

void cb_read_stereo_status(void)
{
    uint8_t byte1, byte2;
    uint8_t request, param;

    static uint8_t sql = 1, volume = 1, channel = 1;
            
    cb_spi_transfer(CB_CMD_GET_STEREO_STATUS);
    byte1 = cb_spi_transfer(0x00);
    byte2 = cb_spi_transfer(0x00);
    debug_print("byte1, byte2 = 0x%02X, 0x%02X\n", byte1, byte2);

    request = byte1 & 0x07U;

    param = (byte1 & CB_STEREO_REQUEST_PARAM_LSB) ? 1 : 0;
    param |= ((byte2 & CB_STEREO_REQUEST_PARAM_MASK) << 1U);

    switch(request)
    {
        case CB_STEREO_REQUEST_VOLUME:
            debug_print("VOLUME %u\n", param);
            volume = param;
            cb_write_radio_status(CB_RADIO_STATUS_VOLUME, volume, false, false);
            
            break;

        case CB_STEREO_REQUEST_CHANNEL:
            debug_print("CHANNEL %u\n", param);
            channel = param;
            cb_write_radio_status(CB_RADIO_STATUS_CHANNEL, channel, false, false);
      
            break;

        case CB_STEREO_REQUEST_SQUELCH:
            debug_print("SQUELCH %u\n", param);
            sql = param;
            cb_write_radio_status(CB_RADIO_STATUS_SQUELCH, sql, false, false);
            
            break;
    }

    debug_print("vol=%u, ch=%u, sql=%u\n", volume, channel, sql);
}


void loop() 
{
    static bool flag = false;
    static bool wait_for_reset = true;
    static bool old_cb_perm_value = false;
    static uint32_t perm_pulse_timer = 0;

    bool cb_so_value;
    bool cb_perm_value;
    uint32_t time_elapsed;

    cb_perm_value = (digitalRead(CB_PERM) == LOW);

    // If there is a rising edge on PERM
    if(!old_cb_perm_value && cb_perm_value)
    {
        if(wait_for_reset)
        {
            perm_pulse_timer = millis();
        }
        else
        {
            cb_read_stereo_status();
            
        }
    }

    if(perm_pulse_timer != 0)
    {
        time_elapsed = millis() - perm_pulse_timer;

        if(time_elapsed > 90U)
        {
            perm_pulse_timer = 0;
            Serial.write("91MS_ELAPSED\n"); 

            cb_write_radio_status(CB_RADIO_STATUS_SQUELCH, 1, false, false);
            cb_write_radio_status(CB_RADIO_STATUS_VOLUME, 1, false, false);
            cb_write_radio_status(CB_RADIO_STATUS_CHANNEL, 1, false, false);

            digitalWrite(CB_SI, LOW);

            wait_for_reset = false;
        }
    }

    // If there is a falling edge on PERM
    if(old_cb_perm_value && !cb_perm_value)
    {
        digitalWrite(CB_SO, LOW);
        perm_pulse_timer = 0;
    }

    



    old_cb_perm_value = cb_perm_value;

}
